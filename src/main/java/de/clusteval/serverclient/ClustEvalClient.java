/*******************************************************************************
 * Copyright (c) 2013 Christian Wiwie.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Christian Wiwie - initial API and implementation
 ******************************************************************************/
package de.clusteval.serverclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.Options;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import de.clusteval.auth.IClientPermissions;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.ISerializableClustering;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.dataset.DataMatrix;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.ISerializableDataSet;
import de.clusteval.data.dataset.ISerializableDataSetConfig;
import de.clusteval.data.dataset.format.DataSetParseException;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.dataset.generator.UnknownDataSetGeneratorException;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.ISerializableGoldStandard;
import de.clusteval.data.goldstandard.ISerializableGoldStandardConfig;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.randomizer.UnknownDataRandomizerException;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.NoRepositoryObjectFinderFoundException;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.UnknownDynamicComponentException;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.IProgramConfig;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ISerializableStandaloneProgram;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.IRun;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.UnknownRunTypeException;
import de.clusteval.run.runnable.IterationRunnableStatus;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ISerializableRunResult;
import de.clusteval.run.runresult.UnknownRunResultException;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DeletionException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.file.FileUtils;
import dk.sdu.imada.compbio.format.Formatter;
import dk.sdu.imada.compbio.utils.Pair;
import dk.sdu.imada.compbio.utils.StringExt;
import dk.sdu.imada.compbio.utils.Triple;

/**
 * @author Christian Wiwie ClustEvalClient
 */
public class ClustEvalClient implements IClustEvalClient {

	/**
	 * The ip address on which this client should look for the server.
	 */
	protected String ip;

	/**
	 * The port on which this client should connect to the server.
	 */
	protected int port;

	/**
	 * The client id this client. Either the client id was specified in the
	 * command line options, or the server told this client its id.
	 * 
	 * <p>
	 * The first scenario is helpful, if a client wants to connect multiple
	 * times to the server. After a client reconnects to the server he is only
	 * able to get the status of those runs, that belong to his client id.
	 * Therefore in this case, the client needs to reconnect with the same id.
	 */
	protected String clientId;

	/**
	 * The server this client connected to.
	 */
	protected IBackendServer server;

	protected static Logger log = LoggerFactory
			.getLogger(ClustEvalClient.class);

	/**
	 * 
	 */
	public ClustEvalClient() {
		super();
	}

	public ClustEvalClient(final String ip, final int port) {
		super();
		this.ip = ip;
		this.port = port;
	}

	public ClustEvalClient(final String ip, final int port,
			final String clientId) {
		super();
		this.ip = ip;
		this.port = port;
		this.clientId = clientId;
	}

	/**
	 * @throws UnknownHostException
	 * @throws ConnectException
	 * @throws IncompatibleClustEvalVersionException
	 * 
	 */
	@Override
	public void connectToServer() throws UnknownHostException, ConnectException,
			IncompatibleClustEvalVersionException {
		try {
			Registry registry;
			if (this.ip == null)
				registry = LocateRegistry.getRegistry(null, this.port);
			else
				registry = LocateRegistry.getRegistry(this.ip, this.port);
			server = (IBackendServer) registry.lookup("EvalServer");
			checkCompatibilityWithServer();
			if (this.clientId == null)
				this.clientId = server.getClientId();
			this.log.debug("Connected to the ClustEval server with Client ID: "
					+ this.clientId);
		} catch (UnknownHostException e) {
			this.log.error("Could not connect to server: " + e.getMessage());
			throw e;
		} catch (ConnectException e) {
			this.log.error("Could not connect to server: " + e.getMessage());
			throw e;
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (IncompatibleClustEvalVersionException e) {
			ComparableVersion actualTargetVersion = new ComparableVersion(
					e.getActualTargetVersion());
			VersionRange targetVersionRequirement = e
					.getTargetVersionRequirement();
			ArtifactVersion lowerBound = targetVersionRequirement
					.getRestrictions().get(0).getLowerBound();
			ArtifactVersion upperBound = targetVersionRequirement
					.getRestrictions().get(0).getUpperBound();

			boolean clientTooOld = actualTargetVersion.compareTo(
					new ComparableVersion(upperBound.toString())) > 0;
			boolean clientTooNew = actualTargetVersion.compareTo(
					new ComparableVersion(lowerBound.toString())) < 0;
			if (clientTooOld)
				this.log.error(String.format(
						"This ClustEval client is too old for the ClustEval server. Supported server versions: %s; given: %s. "
								+ "Please upgrade your client to a newer version",
						targetVersionRequirement, actualTargetVersion));
			else if (clientTooNew)
				this.log.error(String.format(
						"The ClustEval server version is too old. Supported server versions: %s; given: %s. "
								+ "Please upgrade your server to a newer version or use an older client",
						targetVersionRequirement, actualTargetVersion));
			throw e;
		}
	}

	protected static void printClientID(final String clientId) {
		log.info("Your Client ID: " + clientId);
		log.info(
				"Your Client ID is important to remember if your client disconnects from the server and you reconnect later and need access to previous runs and results.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getClientPermissions()
	 */
	public IClientPermissions getClientPermissions()
			throws RemoteException, UnknownClientException {
		IClientPermissions perms = this.server.getClientPermissions(clientId);
		return perms;
	}

	protected void reaquireUserId() throws RemoteException {
		log.info("Reaquiring new Client ID from server ...");
		this.clientId = server.getClientId();
		printClientID(clientId);
	}

	protected void printActiveThreads() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		Map<String, IterationRunnableStatus> activeThreads = this.server
				.getActiveThreads(clientId);
		if (activeThreads.isEmpty())
			System.out.println("No active threads");
		else {
			System.out.println("Active threads:");
			int[] columnWidths = new int[]{5, 15, 25, 25, 25, 25, 25, 20};
			String formatString = String.format(
					"%%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds\n",
					columnWidths[0], columnWidths[1], columnWidths[2],
					columnWidths[3], columnWidths[4], columnWidths[5],
					columnWidths[6], columnWidths[7]);
			String headerLine = String.format(formatString, "#", "Thread",
					"Run", "ProgramConfig", "DataConfig", "Iteration", "State",
					"Running time");
			System.out.print(headerLine);
			System.out.println(StringExt.repeat("-", headerLine.length()));
			List<String> threadNames = new ArrayList<String>(
					activeThreads.keySet());
			Collections.sort(threadNames);
			int i = 1;
			for (String t : threadNames) {
				IterationRunnableStatus value = activeThreads.get(t);
				String[] split1 = value.getParentRunnableName().split(": ");
				String[] split2 = split1[1].split(",");

				String threadNum;
				String threadName;
				String runName;
				String programConfigName;
				String dataConfigName;
				String iterationInfo;
				String runningTime;
				String currentStatus;
				if (value.getCurrentRunnableState() == null)
					currentStatus = "???";
				else
					currentStatus = StringExt.ellipsis(
							value.getCurrentRunnableState(), columnWidths[6]);

				threadNum = StringExt.ellipsis(i++ + "", columnWidths[0]);
				threadName = StringExt.ellipsis(t, columnWidths[1]);
				runName = StringExt.ellipsis(split1[0], columnWidths[2]);
				programConfigName = StringExt.ellipsis(split2[0],
						columnWidths[3]);
				dataConfigName = StringExt.ellipsis(split2[1], columnWidths[4]);
				runningTime = StringExt.ellipsis(
						Formatter.formatMsToDuration(System.currentTimeMillis()
								- value.getStartTime(), false, false),
						columnWidths[7]);

				if (value.getStatus().equals("-1")) {
					iterationInfo = StringExt.ellipsis("isoMDS",
							columnWidths[5]);
				} else if (value.getStatus().equals("-2")) {
					iterationInfo = StringExt.ellipsis("PCA", columnWidths[5]);
				} else
					iterationInfo = StringExt.ellipsis(value.getStatus(),
							columnWidths[5]);
				System.out.format(formatString, threadNum, threadName, runName,
						programConfigName, dataConfigName, iterationInfo,
						currentStatus, runningTime);
			}
		}
	}

	protected void printIterationRunnableQueue() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		List<Triple<String, String, Long>> iterationRunnables = this.server
				.getIterationRunnableQueue(clientId);
		if (iterationRunnables.isEmpty())
			System.out.println("No iteration runnables queued");
		else {
			System.out.println("Iteration runnable queue:");
			int[] columnWidths = new int[]{5, 40, 30, 30, 30, 20};
			String formatString = String.format(
					"%%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds | %%-%ds\n",
					columnWidths[0], columnWidths[1], columnWidths[2],
					columnWidths[3], columnWidths[4], columnWidths[5]);
			String headerLine = String.format(formatString, "#", "Run",
					"ProgramConfig", "DataConfig", "Iteration",
					"Neg. Priority");
			System.out.print(headerLine);
			System.out.println(StringExt.repeat("-", headerLine.length()));
			int i = 1;
			for (Triple<String, String, Long> value : iterationRunnables) {
				String[] split1 = value.getFirst().split(": ");
				String[] split2 = split1[1].split(",");

				String queuePosition;
				String runName;
				String programConfigName;
				String dataConfigName;
				String iterationInfo;
				String priority;

				queuePosition = StringExt.ellipsis(i++ + "", columnWidths[0]);
				runName = StringExt.ellipsis(split1[0], columnWidths[1]);
				programConfigName = StringExt.ellipsis(split2[0],
						columnWidths[2]);
				dataConfigName = StringExt.ellipsis(split2[1], columnWidths[3]);
				priority = StringExt.ellipsis(value.getThird() + "",
						columnWidths[5]);

				if (value.getSecond().equals("-1")) {
					iterationInfo = StringExt.ellipsis("isoMDS",
							columnWidths[4]);
				} else if (value.getSecond().equals("-2")) {
					iterationInfo = StringExt.ellipsis("PCA", columnWidths[4]);
				} else
					iterationInfo = StringExt.ellipsis(value.getSecond(),
							columnWidths[4]);
				System.out.format(formatString, queuePosition, runName,
						programConfigName, dataConfigName, iterationInfo,
						priority);
			}
		}
	}
	//
	// private void addBackendServerSlave(final String[] hostPort) {
	// try {
	// server.addBackendServerSlave(hostPort[0], Integer.valueOf(hostPort[1]));
	// this.log.info("Added slave to server");
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// }
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#shutdownFramework()
	 */
	public void shutdownFramework()
			throws OperationNotPermittedException, UnknownClientException {
		try {
			this.server.shutdown(this.clientId, 0);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getMyRunStatus()
	 */
	public Map<String, Pair<RUN_STATUS, Float>> getMyRunStatus()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getRunStatusForClientId(this.clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getMyEstimatedRuntime()
	 */
	public Map<String, Long> getMyEstimatedRuntime() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getEstimatedRemainingRuntimeForClientId(this.clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getQueue()
	 */
	public Collection<String> getQueue() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getQueue(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getFinishedRuns()
	 */
	public Collection<String> getFinishedRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getFinishedRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRunningRuns()
	 */
	public Collection<String> getRunningRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getRunningRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getTerminatedRuns()
	 */
	public Collection<String> getTerminatedRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getTerminatedRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#forgetRunStatus(java.lang.
	 * String)
	 */
	public void forgetRunStatus(String uniqueRunId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		this.server.forgetRunStatus(clientId, uniqueRunId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRunResults(java.lang.
	 * String)
	 */
	public Map<Pair<String, String>, Map<String, Double>> getRunResults(
			final String uniqueRunIdentifier) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getRunResults(clientId, uniqueRunIdentifier);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getClusteringRunResultIdentifiers()
	 */
	public Collection<String> getClusteringRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getClusteringRunResultIdentifiers(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getParameterOptimizationRunResultIdentifiers()
	 */
	public Collection<String> getParameterOptimizationRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getParameterOptimizationRunResultIdentifiers(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getDataAnalysisRunResultIdentifiers()
	 */
	public Collection<String> getDataAnalysisRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDataAnalysisRunResultIdentifiers(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getRunAnalysisRunResultIdentifiers()
	 */
	public Collection<String> getRunAnalysisRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getRunAnalysisRunResultIdentifiers(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getRunDataAnalysisRunResultIdentifiers()
	 */
	public Collection<String> getRunDataAnalysisRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getRunDataAnalysisRunResultIdentifiers(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getParametersForProgramConfiguration(java.lang.String)
	 */
	public Map<String, Map<String, String>> getParametersForProgramConfiguration(
			final String programConfigName) throws RemoteException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		return server.getParametersForProgramConfiguration(clientId,
				programConfigName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRunResumes()
	 */
	public Collection<String> getRunResumes() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getRunResumes(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getMyOptimizationRunStatus()
	 */
	public Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> getMyOptimizationRunStatus()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getOptimizationRunStatusForClientId(this.clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getClientId()
	 */
	public String getClientId() {
		return this.clientId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRuns()
	 */
	public Collection<ISerializableRun> getRuns()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getRuns(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getPrograms()
	 */
	@Override
	public Collection<ISerializableProgram> getPrograms()
			throws RemoteException, UnknownRProgramException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException,
			RepositoryObjectSerializationException,
			UnknownDataSetFormatException, UnknownRunResultFormatException {
		return server.getPrograms(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataSetGenerators()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDataSetGenerator>> getDataSetGenerators()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDataSetGenerator.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataSets()
	 */
	public Collection<ISerializableDataSet> getDataSets()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getDataSets(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#performRun(java.lang.String)
	 */
	public String performRun(final String runId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		return server.performRun(this.clientId, runId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#resumeRun(java.lang.String)
	 */
	public boolean resumeRun(final String uniqueRunIdentifier)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.resumeRun(this.clientId, uniqueRunIdentifier);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#terminateRun(java.lang.String)
	 */
	public boolean terminateRun(final String runId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.terminateRun(this.clientId, runId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataRandomizers()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDataRandomizer>> getDataRandomizers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDataRandomizer.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getProgramConfigurations()
	 */
	@Override
	public Collection<ISerializableProgramConfig> getProgramConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getProgramConfigurations(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getProgramAliases()
	 */
	public Collection<String> getProgramAliases() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getProgramAliases(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataConfigurations()
	 */
	public Collection<ISerializableDataConfig> getDataConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getDataConfigurations(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getDataConfiguration(java.lang
	 * .String, java.lang.String)
	 */
	public ISerializableDataConfig getDataConfiguration(final String name,
			final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getDataConfiguration(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getProgramConfiguration(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public ISerializableProgramConfig getProgramConfiguration(final String name,
			final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getProgramConfiguration(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRun(java.lang.String,
	 * java.lang.String)
	 */
	public ISerializableRun getRun(final String name, final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getRun(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getProgram(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ISerializableProgram getProgram(final String name,
			final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getProgram(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getDataSet(java.lang.String,
	 * java.lang.String)
	 */
	public ISerializableDataSet getDataSet(final String name,
			final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getDataSet(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getGoldStandard(java.lang.
	 * String, java.lang.String)
	 */
	public ISerializableGoldStandard getGoldStandard(final String name,
			final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getGoldStandard(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getDataSetConfiguration(java.
	 * lang.String, java.lang.String)
	 */
	public ISerializableDataSetConfig getDataSetConfiguration(final String name,
			final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getDataSetConfiguration(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getGoldStandardConfiguration(
	 * java.lang.String, java.lang.String)
	 */
	public ISerializableGoldStandardConfig getGoldStandardConfiguration(
			final String name, final String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getGoldStandardConfiguration(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getDataSetConfigurations()
	 */
	public Collection<ISerializableDataSetConfig> getDataSetConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getDataSetConfigurations(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getStatsOfDataConfiguration(
	 * java.lang.String, java.lang.String)
	 */
	public Map<String, Object> getStatsOfDataConfiguration(final String name,
			final String version) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getStatsOfDataConfiguration(clientId, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getGoldStandards()
	 */
	public Collection<ISerializableGoldStandard> getGoldStandards()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getGoldStandards(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getGoldStandardConfigurations(
	 * )
	 */
	public Collection<ISerializableGoldStandardConfig> getGoldStandardConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return server.getGoldStandardConfigurations(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getClusteringQualityMeasures()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IClusteringQualityMeasure>> getClusteringQualityMeasures()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId,
				IClusteringQualityMeasure.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getParameterOptimizationMethods()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IParameterOptimizationMethod>> getParameterOptimizationMethods()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId,
				IParameterOptimizationMethod.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataStatistics()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDataStatistic>> getDataStatistics()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDataStatistic.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRunStatistics()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IRunStatistic>> getRunStatistics()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IRunStatistic.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRunDataStatistics()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IRunDataStatistic>> getRunDataStatistics()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IRunDataStatistic.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataPreprocessors()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDataPreprocessor>> getDataPreprocessors()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDataPreprocessor.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDistanceMeasures()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDistanceMeasure>> getDistanceMeasures()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDistanceMeasure.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataSetFormats()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDataSetFormat>> getDataSetFormats()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDataSetFormat.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getRunResultFormats()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IRunResultFormat>> getRunResultFormats()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IRunResultFormat.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getDataSetTypes()
	 */
	public Collection<ISerializableWrapperDynamicComponent<IDataSetType>> getDataSetTypes()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException {
		return server.getDynamicComponents(clientId, IDataSetType.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getAbsoluteRepositoryPath()
	 */
	public String getAbsoluteRepositoryPath() throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getAbsoluteRepositoryPath(clientId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getRunExceptions(java.lang.
	 * String)
	 */
	public Collection<String> getRunExceptions(final String runId,
			final boolean getWarningExceptions) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getRunExceptionMessages(clientId, runId,
				getWarningExceptions);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#clearRunExceptions(java.lang.
	 * String)
	 */
	public void clearRunExceptions(final String runId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		this.server.clearRunExceptions(clientId, runId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getParsingExceptionMessages(
	 * java.lang.Class)
	 */
	public Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>> getParsingExceptionMessages(
			final Class<? extends IRepositoryObject>... clazz)
			throws RemoteException, OperationNotPermittedException,
			OperationNotPermittedException, UnknownClientException {
		return server.getParsingExceptionMessages(clientId, clazz);
	}

	/**
	 * This method is responsible for creating all the appender that are added
	 * to the logger.
	 * <p>
	 * Three appenders are created:
	 * <ul>
	 * <li><b>ConsoleAppender</b>: Writes the logging output to the standard out
	 * </li>
	 * <li><b>FileAppender</b>: Writes the logging output as formatter text to
	 * the file clustevalClient.log</li>
	 * <li><b>FileAppender</b>: Writes the logging output in lilith binary
	 * format to the file clustevalClient.lilith</li>
	 * </ul>
	 * 
	 * @param cmd
	 *            The command line parameters including possible options of
	 *            logging
	 */
	protected static void initLogging(Level logLevel) {
		Logger log = LoggerFactory.getLogger(ClustEvalClient.class);

		ch.qos.logback.classic.Logger logger = ((ch.qos.logback.classic.Logger) LoggerFactory
				.getLogger(Logger.ROOT_LOGGER_NAME));
		logger.setLevel(logLevel);

		// file appender for clustevalServer.log plaintext file
		FileAppender<ILoggingEvent> fileApp = new FileAppender<ILoggingEvent>();
		fileApp.setName("clientLogFile");
		String logFilePath = FileUtils.buildPath(System.getProperty("user.dir"),
				"clustevalClient.log");
		fileApp.setFile(logFilePath);

		fileApp.setAppend(true);
		fileApp.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		fileApp.setEncoder(new PatternLayoutEncoder());
		PatternLayout layout = new PatternLayout();
		layout.setPattern(
				"%date{dd MMM yyyy HH:mm:ss.SSS} [%thread] %-5level %logger{35} - %msg%n");
		layout.setContext((LoggerContext) LoggerFactory.getILoggerFactory());
		layout.start();
		fileApp.setLayout(layout);
		fileApp.start();
		logger.addAppender(fileApp);

		log.debug("Using log level " + logLevel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#uploadDataSet(de.clusteval.
	 * data.dataset.ISerializableDataSet, byte[])
	 */
	@Override
	public boolean uploadDataSet(final ISerializableDataSet dataSet,
			final byte[] bytesDatasetFile,
			final List<byte[]> bytesAdditionalDatasetFiles)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RegisterException,
			DynamicComponentInitializationException,
			UnknownDynamicComponentException, DeserializationException,
			DataSetParseException {
		return this.server.uploadDataset(this.clientId, dataSet,
				bytesDatasetFile, bytesAdditionalDatasetFiles);
	}

	@Override
	public boolean createNewDatasetWithAttributesFromExisting(
			final ISerializableDataSet<IDataSet> dataSet,
			final ISerializableDataSet<IDataSet> existingDataSet)
			throws DeserializationException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DataSetParseException {
		return this.server.createNewDatasetWithAttributesFromExisting(clientId,
				dataSet, existingDataSet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#uploadGoldStandard(de.
	 * clusteval.data.goldstandard.ISerializableGoldStandard, byte[])
	 */
	public boolean uploadGoldStandard(
			final ISerializableGoldStandard goldStandard, byte[] bytes)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RegisterException,
			DynamicComponentInitializationException,
			UnknownDynamicComponentException, DeserializationException {
		return this.server.uploadGoldStandard(this.clientId, goldStandard,
				bytes);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#uploadDataConfig(de.clusteval.
	 * data.ISerializableDataConfig)
	 */
	public boolean uploadDataConfig(final ISerializableDataConfig dataConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RepositoryObjectDumpException,
			UnknownDynamicComponentException,
			DynamicComponentInitializationException, RegisterException,
			DeserializationException {
		return this.server.uploadDataConfig(this.clientId, dataConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#uploadDataSetConfig(de.
	 * clusteval.data.dataset.ISerializableDataSetConfig)
	 */
	public boolean uploadDataSetConfig(
			final ISerializableDataSetConfig datasetConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectDumpException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			UnknownDynamicComponentException,
			DynamicComponentInitializationException, RegisterException,
			DeserializationException {
		return this.server.uploadDataSetConfig(this.clientId, datasetConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#uploadGoldStandardConfig(de.
	 * clusteval.data.goldstandard.ISerializableGoldStandardConfig)
	 */
	public boolean uploadGoldStandardConfig(
			final ISerializableGoldStandardConfig goldstandardConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectDumpException,
			DynamicComponentInitializationException,
			UnknownDynamicComponentException, RegisterException,
			DeserializationException {
		return this.server.uploadGoldStandardConfig(this.clientId,
				goldstandardConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#uploadRun(de.clusteval.run.
	 * ISerializableRun)
	 */
	public boolean uploadRun(final ISerializableRun run)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, DeserializationException, RegisterException,
			RepositoryObjectDumpException {
		return this.server.uploadRun(this.clientId, run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#uploadProgramConfig(de.
	 * clusteval.program.ISerializableProgramConfig)
	 */
	public boolean uploadProgramConfig(
			final ISerializableProgramConfig programConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, DeserializationException, RegisterException,
			RepositoryObjectDumpException {
		return this.server.uploadProgramConfig(this.clientId, programConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteDataset(de.clusteval.
	 * data.dataset.ISerializableDataSet)
	 */
	public boolean deleteDataset(final ISerializableDataSet dataSet)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.server.deleteDataset(clientId, dataSet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteDataConfiguration(de.
	 * clusteval.data.ISerializableDataConfig)
	 */
	public boolean deleteDataConfiguration(
			final ISerializableDataConfig dataConfig) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		return this.server.deleteDataConfiguration(clientId, dataConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteDataSetConfiguration(de.
	 * clusteval.data.dataset.ISerializableDataSetConfig)
	 */
	public boolean deleteDataSetConfiguration(
			final ISerializableDataSetConfig datasetConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.server.deleteDataSetConfiguration(clientId, datasetConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * deleteGoldStandardConfiguration(de.clusteval.data.goldstandard.
	 * ISerializableGoldStandardConfig)
	 */
	public boolean deleteGoldStandardConfiguration(
			final ISerializableGoldStandardConfig goldstandardConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.server.deleteGoldStandardConfiguration(clientId,
				goldstandardConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#deleteGoldStandard(de.
	 * clusteval.data.goldstandard.ISerializableGoldStandard)
	 */
	public boolean deleteGoldStandard(
			final ISerializableGoldStandard goldStandard)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.server.deleteGoldStandard(clientId, goldStandard);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteProgram(de.clusteval.
	 * program.ISerializableProgram)
	 */
	public boolean deleteProgram(final ISerializableProgram program)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.server.deleteProgram(clientId, program);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteProgramConfiguration(de.
	 * clusteval.program.ISerializableProgramConfig)
	 */
	public boolean deleteProgramConfiguration(
			final ISerializableProgramConfig programConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException {
		return this.server.deleteProgramConfiguration(clientId, programConfig);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteRun(de.clusteval.run.
	 * ISerializableRun)
	 */
	public boolean deleteRun(final ISerializableRun run) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException {
		return this.server.deleteRun(clientId, run);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteRunResult(java.lang.
	 * String)
	 */
	public boolean deleteRunResult(final String name)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException {
		return this.server.deleteRunResult(clientId, name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#uploadProgram(de.clusteval.
	 * program.ISerializableProgram, byte[])
	 */
	@Override
	public boolean uploadStandaloneProgram(
			final ISerializableStandaloneProgram program, final byte[] bytes)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RegisterException,
			DynamicComponentInitializationException,
			UnknownDynamicComponentException, DeserializationException,
			RepositoryObjectParseException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException,
			NoRepositoryObjectFinderFoundException {
		return this.server.uploadStandaloneProgram(this.clientId, program,
				bytes);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getErrorRuns()
	 */
	public Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> getErrorRuns()
			throws ObjectNotFoundException, ObjectVersionNotFoundException,
			RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> errorRuns = this.server
				.getErrorObjects(clientId, IRun.class);
		return errorRuns;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * getNextVersionForRepositoryObject(java.lang.Class, java.lang.String,
	 * java.lang.String)
	 */
	public String getNextVersionForRepositoryObject(
			final Class<? extends IRepositoryObject> clazz, final String name,
			final String currentVersion) throws RemoteException,
			OperationNotPermittedException, UnknownClientException {
		return server.getNextVersionForRepositoryObject(clientId, clazz, name,
				currentVersion);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#isRelativeDataSetFormat(de.
	 * clusteval.data.dataset.format.ISerializableDataSetFormat)
	 */
	public boolean isRelativeDataSetFormat(ISerializableDataSetFormat format)
			throws RemoteException, DeserializationException,
			OperationNotPermittedException, UnknownClientException {
		return server.isRelativeDataSetFormat(clientId, format);
	}

	protected void checkCompatibilityWithServer()
			throws IncompatibleClustEvalVersionException {
		ClassVersionRequirement clientAnno = IClustEvalClient.class
				.getAnnotation(ClassVersionRequirement.class);
		if (clientAnno == null) {
			this.log.warn(
					"Could not check for version compatibility with the ClustEval server: Could not parse version specification of ClustEval client");
			// TODO: should we really just return?
			return;
		}

		try {
			VersionRange clientVersionRequirement = VersionRange
					.createFromVersionSpec(clientAnno.versionSpecification());

			ArtifactVersion serverAnno = new DefaultArtifactVersion(
					this.server.getClustEvalServerVersion(clientId));
			if (serverAnno == null)
				throw new IncompatibleClustEvalVersionException(
						IClustEvalClient.class.getSimpleName(),
						clientVersionRequirement, clientAnno.target(),
						"< 1.6.3");

			if (!(clientVersionRequirement.containsVersion(serverAnno)))
				throw new IncompatibleClustEvalVersionException(
						ClustEvalClient.class.getSimpleName(),
						clientVersionRequirement, clientAnno.target(),
						serverAnno.toString());
		} catch (IncompatibleClustEvalVersionException e) {
			throw e;
		} catch (Exception e) {
			try {
				throw new IncompatibleClustEvalVersionException(
						ClustEvalClient.class.getSimpleName(),
						VersionRange
								.createFromVersionSpec(IClustEvalClient.class
										.getAnnotation(
												ClassVersionRequirement.class)
										.versionSpecification()),
						clientAnno.target(), "< 1.6.3");
			} catch (InvalidVersionSpecificationException e1) {
				this.log.warn(
						"Could not check for version compatibility with the ClustEval server: Could not parse version specification of ClustEval client");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#deleteDynamicComponent(java.
	 * lang.Class, java.lang.String, java.lang.String)
	 */
	public void deleteDynamicComponent(
			Class<? extends IRepositoryObjectDynamicComponent> clazz,
			String name, String version) throws RemoteException,
			OperationNotPermittedException, UnknownDynamicComponentException,
			UnknownClientException, DeletionException {
		this.server.deleteDynamicComponent(clientId, clazz, name, version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getRunResult(java.lang.String)
	 */
	public Collection<ISerializableRunResult<? extends IRunResult>> getRunResult(
			String uniqueRunIdentifier)
			throws RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException {
		return this.server.getRunResult(clientId, uniqueRunIdentifier);
	}

	public ISerializableClustering getClusteringOfParameterOptimizationIteration(
			final String resultId, final String programConfig,
			final String dataConfig, final long iteration)
			throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException {
		return this.server.getClusteringOfParameterOptimizationIteration(
				clientId, resultId, programConfig, dataConfig, iteration);
	}

	public ISerializableClustering getClusteringOfClusteringRun(
			final String resultId, final String programConfig,
			final String dataConfig) throws UnknownRunResultException,
			RepositoryObjectSerializationException, RemoteException {
		return this.server.getClusteringOfClusteringRun(clientId, resultId,
				programConfig, dataConfig);
	}

	public DataMatrix getPCAOfDataConfig(final String resultId,
			final String programConfig, final String dataConfig,
			final int numberPCs) throws UnknownRunResultException,
			RepositoryObjectSerializationException, IOException {
		return this.server.getPCAOfDataConfig(clientId, resultId, programConfig,
				dataConfig, numberPCs);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.clusteval.serverclient.IClustEvalClient#getRunTypeOfRunResult(java.
	 * lang.String)
	 */
	public RUN_TYPE getRunTypeOfRunResult(String uniqueRunId)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, UnknownRunTypeException,
			UnknownRunResultException {
		return this.server.getRunTypeOfRunResult(clientId, uniqueRunId);
	}

	/**
	 * 
	 * @param randomizerName
	 *            The simple name of the class of the data randomizer.
	 * @return A wrapper objects keeping all the options of the specified data
	 *         randomizer.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataRandomizerException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	public Options getOptionsForDataRandomizer(final String randomizerName)
			throws RemoteException, UnknownDataRandomizerException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException {
		return server.getOptionsForDataRandomizer(clientId, randomizerName);
	}

	/**
	 * 
	 * @param generatorName
	 *            The simple name of the class of the dataset generator.
	 * @return A wrapper objects keeping all the options of the specified
	 *         dataset generator.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownDataSetGeneratorException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	public Options getOptionsForDataSetGenerator(final String generatorName)
			throws RemoteException, UnknownDataSetGeneratorException,
			DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException {
		return server.getOptionsForDataSetGenerator(clientId, generatorName);
	}

	@Override
	public <T extends IRepositoryObject> ISerializableWrapperDynamicComponent uploadDynamicComponent(
			Class<T> clazz, final String jarFileName, byte[] jarAsBytes)
			throws OperationNotPermittedException, IOException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RepositoryObjectParseException,
			RegisterException {
		return server.uploadDynamicComponent(clientId, clazz, jarFileName,
				jarAsBytes);
	}

	public static ArtifactVersion getClientVersion() {
		return new DefaultArtifactVersion(IClustEvalClient.class
				.getAnnotation(ClassVersion.class).version());
	}

	@Override
	public ComparableVersion checkForServerUpdate(
			boolean includeSnapshotVersions) throws RemoteException {
		String v = this.server.checkForServerUpdate(clientId,
				includeSnapshotVersions);
		if (v == null)
			return null;
		return new ComparableVersion(v);
	}

	/**
	 * 
	 * @return The newer version if available, null otherwise.
	 */
	@Override
	public ComparableVersion checkForUpdate(boolean includeSnapshotVersions)
			throws RemoteException {
		String v = this.server.checkForClientUpdate(
				clientId, IClustEvalClient.class
						.getAnnotation(ClassVersion.class).version(),
				includeSnapshotVersions);
		if (v == null)
			return null;
		return new ComparableVersion(v);
	}

	@Override
	public Map<String, Map<String, String>> checkForComponentUpdates(
			boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException {
		return this.server.checkForComponentUpdates(clientId,
				includeSnapshotVersions);
	}
	//
	// public String getComponentVersionReleaseTitle() {
	// return this.server
	// }
	//
	// public String getComponentVersionReleaseNotes() {
	//
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#
	 * ensureRuntimeInformationForAllProgramConfigs()
	 */
	@Override
	public void ensureRuntimeInformationForAllProgramConfigs()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, URISyntaxException, InterruptedException,
			RemoteException {
		this.server.ensureRuntimeInformationForAllProgramConfigs();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getExpectedRuntime(de.
	 * clusteval.program.IProgramConfig, int, int, int)
	 */
	@Override
	public long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings, int numberCores)
			throws RemoteException, DeserializationException {
		return this.server.getExpectedRuntime(programConfig,
				numberSamplesInDataset, numberClusterings, numberCores);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.clusteval.serverclient.IClustEvalClient#getExpectedRuntime(de.
	 * clusteval.program.IProgramConfig, int)
	 */
	@Override
	public long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings)
			throws RemoteException, DeserializationException {
		return this.server.getExpectedRuntime(programConfig,
				numberSamplesInDataset, numberClusterings);
	}
}
