/**
 * 
 */
package de.clusteval.serverclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.UnknownHostException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.maven.artifact.versioning.ComparableVersion;

import de.clusteval.auth.IClientPermissions;
import de.clusteval.auth.OperationNotPermittedException;
import de.clusteval.auth.UnknownClientException;
import de.clusteval.cluster.paramOptimization.IParameterOptimizationMethod;
import de.clusteval.cluster.quality.IClusteringQualityMeasure;
import de.clusteval.cluster.quality.UnknownClusteringQualityMeasureException;
import de.clusteval.context.UnknownContextException;
import de.clusteval.data.ISerializableDataConfig;
import de.clusteval.data.dataset.IDataSet;
import de.clusteval.data.dataset.ISerializableDataSet;
import de.clusteval.data.dataset.ISerializableDataSetConfig;
import de.clusteval.data.dataset.format.DataSetParseException;
import de.clusteval.data.dataset.format.IDataSetFormat;
import de.clusteval.data.dataset.format.ISerializableDataSetFormat;
import de.clusteval.data.dataset.format.UnknownDataSetFormatException;
import de.clusteval.data.dataset.generator.IDataSetGenerator;
import de.clusteval.data.dataset.type.IDataSetType;
import de.clusteval.data.dataset.type.UnknownDataSetTypeException;
import de.clusteval.data.distance.IDistanceMeasure;
import de.clusteval.data.distance.UnknownDistanceMeasureException;
import de.clusteval.data.goldstandard.ISerializableGoldStandard;
import de.clusteval.data.goldstandard.ISerializableGoldStandardConfig;
import de.clusteval.data.preprocessing.IDataPreprocessor;
import de.clusteval.data.randomizer.IDataRandomizer;
import de.clusteval.data.statistics.IDataStatistic;
import de.clusteval.framework.repository.DeserializationException;
import de.clusteval.framework.repository.IRepositoryObject;
import de.clusteval.framework.repository.IRepositoryObjectDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperDynamicComponent;
import de.clusteval.framework.repository.ISerializableWrapperRepositoryObject;
import de.clusteval.framework.repository.NoRepositoryObjectFinderFoundException;
import de.clusteval.framework.repository.ObjectNotFoundException;
import de.clusteval.framework.repository.ObjectNotRegisteredException;
import de.clusteval.framework.repository.ObjectVersionNotFoundException;
import de.clusteval.framework.repository.ObjectVersionNotRegisteredException;
import de.clusteval.framework.repository.RegisterException;
import de.clusteval.framework.repository.RepositoryObjectDumpException;
import de.clusteval.framework.repository.RepositoryObjectSerializationException;
import de.clusteval.framework.repository.UnknownDynamicComponentException;
import de.clusteval.framework.repository.maven.InvalidDynamicComponentNameException;
import de.clusteval.framework.repository.parse.RepositoryObjectParseException;
import de.clusteval.program.ISerializableProgram;
import de.clusteval.program.ISerializableProgramConfig;
import de.clusteval.program.ISerializableStandaloneProgram;
import de.clusteval.program.r.UnknownRProgramException;
import de.clusteval.run.ISerializableRun;
import de.clusteval.run.RUN_STATUS;
import de.clusteval.run.RUN_TYPE;
import de.clusteval.run.UnknownRunTypeException;
import de.clusteval.run.runresult.IRunResult;
import de.clusteval.run.runresult.ISerializableRunResult;
import de.clusteval.run.runresult.UnknownRunResultException;
import de.clusteval.run.runresult.format.IRunResultFormat;
import de.clusteval.run.runresult.format.UnknownRunResultFormatException;
import de.clusteval.run.statistics.IRunDataStatistic;
import de.clusteval.run.statistics.IRunStatistic;
import de.clusteval.utils.ClassVersion;
import de.clusteval.utils.ClassVersionRequirement;
import de.clusteval.utils.DeletionException;
import de.clusteval.utils.DynamicComponentInitializationException;
import de.clusteval.utils.IncompatibleClustEvalVersionException;
import dk.sdu.imada.compbio.utils.Pair;

/**
 * @author Christian Wiwie
 *
 *         <dl>
 *         <dt>8.0</dt>
 *         <dd>Using compbio utils instead of Wiutils</dd>
 *         <dt>&#60; 8.1</dt>
 *         <dd>...</dd>
 *         </dl>
 */
@ClassVersion(version = "8.0")
@ClassVersionRequirement(target = IBackendServer.class, versionSpecification = "[8.0, 8.1)")
public interface IClustEvalClient {

	/**
	 * @throws UnknownClientException
	 * @throws RemoteException
	 * 
	 */
	IClientPermissions getClientPermissions()
			throws RemoteException, UnknownClientException;

	/**
	 * This method will tell the server to shutdown the framework immediately (0
	 * timeout).
	 * 
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 */
	void shutdownFramework()
			throws OperationNotPermittedException, UnknownClientException;

	/**
	 * This method retrieves the status of all the runs of this client.
	 * 
	 * @return A map containing the status as well as the percentage (if) for
	 *         every run, this client has scheduled.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Map<String, Pair<RUN_STATUS, Float>> getMyRunStatus()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * This method retrieves the estimated remaining runtime of all the runs of
	 * this client.
	 * 
	 * @return A map containing the estimated remaining runtime for every run,
	 *         this client has scheduled.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Map<String, Long> getMyEstimatedRuntime() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * @return A collection with the names of all runs and run results that are
	 *         currently enqueued but not yet running.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getQueue() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Collection<String> getFinishedRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Collection<String> getRunningRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Collection<String> getTerminatedRuns() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	void forgetRunStatus(String uniqueRunId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * 
	 * @param uniqueRunIdentifier
	 *            The unique run identifier of a run result stored in the
	 *            corresponding directory of the repository.
	 * @return The run results for the given unique run identifier.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Map<Pair<String, String>, Map<String, Double>> getRunResults(
			String uniqueRunIdentifier) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of the clustering results.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getClusteringRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of the parameter optimization
	 *         results.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getParameterOptimizationRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of the data analysis results.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getDataAnalysisRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of the run analysis results.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getRunAnalysisRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of the run-data analysis results.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getRunDataAnalysisRunResultIdentifiers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Map<String, Map<String, String>> getParametersForProgramConfiguration(
			String programConfigName) throws RemoteException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	/**
	 * @return A collection with the names of all run result directories
	 *         contained in the repository of this server. Those run result
	 *         directories can be resumed, if they were terminated before.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<String> getRunResumes() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Map<String, Pair<Pair<RUN_STATUS, Float>, Map<Pair<String, String>, Pair<Double, Map<String, Pair<Map<String, String>, String>>>>>> getMyOptimizationRunStatus()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return The id of this client.
	 */
	String getClientId();

	/**
	 * 
	 * @return A collection with all runs contained in the server's repository.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<ISerializableRun> getRuns()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	/**
	 * @return A collection with all programs contained in the server's
	 *         repository.
	 * @throws RemoteException
	 * @throws DynamicComponentInitializationException
	 * @throws UnknownRProgramException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 * @throws RepositoryObjectSerializationException
	 * @throws UnknownRunResultFormatException
	 * @throws UnknownDataSetFormatException
	 */
	Collection<ISerializableProgram> getPrograms() throws RemoteException,
			UnknownRProgramException, DynamicComponentInitializationException,
			OperationNotPermittedException, UnknownClientException,
			RepositoryObjectSerializationException,
			UnknownDataSetFormatException, UnknownRunResultFormatException;

	/**
	 * 
	 * @return A collection with the names of all dataset generators registered
	 *         at the repository of this server.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<ISerializableWrapperDynamicComponent<IDataSetGenerator>> getDataSetGenerators()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	/**
	 * @return A collection with all datasets contained in the server's
	 *         repository.
	 * @throws RemoteException
	 *             the remote exception
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<ISerializableDataSet> getDataSets()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	/**
	 * This method tells the server that this client wants to perform the
	 * specified run.
	 * 
	 * @param runId
	 *            The id of the run to be performed
	 * @return The unique identification string of the run; null if it has not
	 *         been scheduled.
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 * @throws ObjectNotRegisteredException
	 */
	String performRun(String runId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	/**
	 * This method tells the server that this client wants to resume the run
	 * that corresponds to the run results folder with the specified unique
	 * identifier.
	 * 
	 * @param uniqueRunIdentifier
	 *            The identifier of the run result that should be resumed.
	 * @return True, if successful
	 * @throws RemoteException
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 */
	boolean resumeRun(String uniqueRunIdentifier) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * This method tells the server, that this client wants to terminate the
	 * specified run.
	 * 
	 * <p>
	 * This method only succeeds, if the run has been started before and is
	 * currently being performed.
	 * 
	 * @param runId
	 *            The id of the run to be terminated.
	 * @return true, if successful
	 * @throws RemoteException
	 *             the remote exception
	 * @throws OperationNotPermittedException
	 * @throws UnknownClientException
	 */
	boolean terminateRun(String runId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	/**
	 * 
	 * @return A collection with the names of all data randomizers registered at
	 *         the repository of this server.
	 * @throws RemoteException
	 * @throws UnknownClientException
	 * @throws OperationNotPermittedException
	 */
	Collection<ISerializableWrapperDynamicComponent<IDataRandomizer>> getDataRandomizers()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableProgramConfig> getProgramConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	Collection<String> getProgramAliases() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Collection<ISerializableDataConfig> getDataConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableDataConfig getDataConfiguration(String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableProgramConfig getProgramConfiguration(String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableRun getRun(String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableProgram getProgram(String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableDataSet getDataSet(String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableGoldStandard getGoldStandard(String name, String version)
			throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableDataSetConfig getDataSetConfiguration(String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	ISerializableGoldStandardConfig getGoldStandardConfiguration(String name,
			String version) throws RemoteException, ObjectNotFoundException,
			ObjectVersionNotFoundException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	Collection<ISerializableDataSetConfig> getDataSetConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	Map<String, Object> getStatsOfDataConfiguration(String name, String version)
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableGoldStandard> getGoldStandards()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	Collection<ISerializableGoldStandardConfig> getGoldStandardConfigurations()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	Collection<ISerializableWrapperDynamicComponent<IClusteringQualityMeasure>> getClusteringQualityMeasures()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IParameterOptimizationMethod>> getParameterOptimizationMethods()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IDataStatistic>> getDataStatistics()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IRunStatistic>> getRunStatistics()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IRunDataStatistic>> getRunDataStatistics()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IDataPreprocessor>> getDataPreprocessors()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IDistanceMeasure>> getDistanceMeasures()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IDataSetFormat>> getDataSetFormats()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IRunResultFormat>> getRunResultFormats()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	Collection<ISerializableWrapperDynamicComponent<IDataSetType>> getDataSetTypes()
			throws RemoteException, OperationNotPermittedException,
			UnknownClientException;

	String getAbsoluteRepositoryPath() throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Collection<String> getRunExceptions(String runId,
			final boolean getWarningExceptions) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	void clearRunExceptions(String runId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	Map<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>, List<String>> getParsingExceptionMessages(
			Class<? extends IRepositoryObject>... clazz)
			throws RemoteException, OperationNotPermittedException,
			OperationNotPermittedException, UnknownClientException;

	boolean uploadDataSet(ISerializableDataSet dataSet,
			final byte[] bytesDatasetFile,
			final List<byte[]> bytesAdditionalDatasetFiles)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RegisterException,
			DynamicComponentInitializationException,
			UnknownDynamicComponentException, DeserializationException,
			DataSetParseException;

	boolean uploadGoldStandard(ISerializableGoldStandard goldStandard,
			byte[] bytes) throws RemoteException, IOException,
			OperationNotPermittedException, UnknownClientException,
			RegisterException, DynamicComponentInitializationException,
			UnknownDynamicComponentException, DeserializationException;

	boolean uploadDataConfig(ISerializableDataConfig dataConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RepositoryObjectDumpException,
			UnknownDynamicComponentException,
			DynamicComponentInitializationException, RegisterException,
			DeserializationException;

	boolean uploadDataSetConfig(ISerializableDataSetConfig datasetConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectDumpException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			UnknownDynamicComponentException,
			DynamicComponentInitializationException, RegisterException,
			DeserializationException;

	boolean uploadGoldStandardConfig(
			ISerializableGoldStandardConfig goldstandardConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectDumpException,
			DynamicComponentInitializationException,
			UnknownDynamicComponentException, RegisterException,
			DeserializationException;

	boolean uploadRun(ISerializableRun run)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, DeserializationException, RegisterException,
			RepositoryObjectDumpException;

	boolean uploadProgramConfig(ISerializableProgramConfig programConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, DeserializationException, RegisterException,
			RepositoryObjectDumpException;

	boolean deleteDataset(ISerializableDataSet dataSet) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	boolean deleteDataConfiguration(ISerializableDataConfig dataConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	boolean deleteDataSetConfiguration(ISerializableDataSetConfig datasetConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	boolean deleteGoldStandardConfiguration(
			ISerializableGoldStandardConfig goldstandardConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	boolean deleteGoldStandard(ISerializableGoldStandard goldStandard)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	boolean deleteProgram(ISerializableProgram program) throws RemoteException,
			IOException, OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	boolean deleteProgramConfiguration(ISerializableProgramConfig programConfig)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException;

	boolean deleteRun(ISerializableRun run) throws RemoteException, IOException,
			OperationNotPermittedException, UnknownClientException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException;

	boolean deleteRunResult(String name)
			throws RemoteException, IOException, OperationNotPermittedException,
			UnknownClientException, ObjectNotRegisteredException;

	boolean uploadStandaloneProgram(ISerializableStandaloneProgram program,
			byte[] bytes) throws RemoteException, IOException,
			OperationNotPermittedException, UnknownClientException,
			RegisterException, DynamicComponentInitializationException,
			UnknownDynamicComponentException, DeserializationException,
			RepositoryObjectParseException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException,
			NoRepositoryObjectFinderFoundException;

	Set<ISerializableWrapperRepositoryObject<? extends IRepositoryObject>> getErrorRuns()
			throws ObjectNotFoundException, ObjectVersionNotFoundException,
			RemoteException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	String getNextVersionForRepositoryObject(
			Class<? extends IRepositoryObject> clazz, String name,
			String currentVersion) throws RemoteException,
			OperationNotPermittedException, UnknownClientException;

	boolean isRelativeDataSetFormat(ISerializableDataSetFormat format)
			throws RemoteException, DeserializationException,
			OperationNotPermittedException, UnknownClientException;

	void deleteDynamicComponent(
			Class<? extends IRepositoryObjectDynamicComponent> clazz,
			String name, String version) throws RemoteException,
			OperationNotPermittedException, UnknownDynamicComponentException,
			UnknownClientException, DeletionException;

	Collection<ISerializableRunResult<? extends IRunResult>> getRunResult(
			String uniqueRunIdentifier)
			throws RemoteException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, OperationNotPermittedException,
			UnknownClientException, RepositoryObjectSerializationException;

	RUN_TYPE getRunTypeOfRunResult(String uniqueRunId) throws RemoteException,
			OperationNotPermittedException, UnknownClientException,
			UnknownRunTypeException, UnknownRunResultException;

	void connectToServer() throws UnknownHostException, ConnectException,
			IncompatibleClustEvalVersionException;

	<T extends IRepositoryObject> ISerializableWrapperDynamicComponent uploadDynamicComponent(
			Class<T> clazz, final String jarFileName, byte[] jarAsBytes)
			throws OperationNotPermittedException, IOException,
			UnknownClientException, ObjectNotRegisteredException,
			ObjectVersionNotRegisteredException, RepositoryObjectParseException,
			RegisterException;

	boolean createNewDatasetWithAttributesFromExisting(
			final ISerializableDataSet<IDataSet> dataSet,
			final ISerializableDataSet<IDataSet> existingDataSet)
			throws DeserializationException, UnknownClientException,
			OperationNotPermittedException, IOException, RegisterException,
			DataSetParseException;

	ComparableVersion checkForServerUpdate(boolean includeSnapshotVersions)
			throws RemoteException;

	/**
	 * @param includeSnapshotVersions
	 * @return
	 * @throws RemoteException
	 */
	ComparableVersion checkForUpdate(boolean includeSnapshotVersions)
			throws RemoteException;

	Map<String, Map<String, String>> checkForComponentUpdates(
			boolean includeSnapshotVersions)
			throws RemoteException, InvalidDynamicComponentNameException;

	void ensureRuntimeInformationForAllProgramConfigs()
			throws UnknownContextException,
			UnknownClusteringQualityMeasureException,
			UnknownDataSetFormatException, UnknownDataSetTypeException,
			UnknownDistanceMeasureException, RepositoryObjectDumpException,
			DynamicComponentInitializationException, RegisterException,
			ObjectNotRegisteredException, ObjectVersionNotRegisteredException,
			IOException, URISyntaxException, InterruptedException,
			RemoteException;

	long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings)
			throws RemoteException, DeserializationException;

	long getExpectedRuntime(ISerializableProgramConfig<?> programConfig,
			int numberSamplesInDataset, int numberClusterings, int numberCores)
			throws RemoteException, DeserializationException;

}